# logicPlan.py
# ------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In logicPlan.py, you will implement logic planning methods which are called by
Pacman agents (in logicAgents.py).
"""

from typing import Dict, List, Tuple, Callable, Generator, Any
import util
import sys
import logic
import game

from logic import conjoin, disjoin
from logic import PropSymbolExpr, Expr, to_cnf, pycoSAT, parseExpr, pl_true

import itertools
import copy

pacman_str = 'P'
food_str = 'FOOD'
wall_str = 'WALL'
pacman_wall_str = pacman_str + wall_str
ghost_pos_str = 'G'
ghost_east_str = 'GE'
pacman_alive_str = 'PA'
DIRECTIONS = ['North', 'South', 'East', 'West']
blocked_str_map = dict([(direction, (direction + "_blocked").upper()) for direction in DIRECTIONS])
geq_num_adj_wall_str_map = dict([(num, "GEQ_{}_adj_walls".format(num)) for num in range(1, 4)])
DIR_TO_DXDY_MAP = {'North':(0, 1), 'South':(0, -1), 'East':(1, 0), 'West':(-1, 0)}


#______________________________________________________________________________
# QUESTION 1

def sentence1() -> Expr:
    """Returns a Expr instance that encodes that the following expressions are all true.
    
    A or B
    (not A) if and only if ((not B) or C)
    (not A) or (not B) or C
    """
    "*** BEGIN YOUR CODE HERE ***"

    A = Expr('A')
    B = Expr('B')
    C = Expr('C')

    s1 = A | B
    s2 = ~A % (~B | C)
    s3 = disjoin(~A, ~B, C)

    return conjoin(s1, s2, s3)

    "*** END YOUR CODE HERE ***"


def sentence2() -> Expr:
    """Returns a Expr instance that encodes that the following expressions are all true.
    
    C if and only if (B or D)
    A implies ((not B) and (not D))
    (not (B and (not C))) implies A
    (not D) implies C
    """

    "*** BEGIN YOUR CODE HERE ***"

    A = Expr('A')
    B = Expr('B')
    C = Expr('C')
    D = Expr('D')

    s1 = C % (B | D)
    s2 = A >> (~B & ~D)
    s3 = ~(B & ~C) >> A
    s4 = ~D >> C

    return conjoin(s1, s2, s3, s4)

    "*** END YOUR CODE HERE ***"


def sentence3() -> Expr:
    """Using the symbols PacmanAlive_1 PacmanAlive_0, PacmanBorn_0, and PacmanKilled_0,
    created using the PropSymbolExpr constructor, return a PropSymbolExpr
    instance that encodes the following English sentences (in this order):

    Pacman is alive at time 1 if and only if Pacman was alive at time 0 and it was
    not killed at time 0 or it was not alive at time 0 and it was born at time 0.

    Pacman cannot both be alive at time 0 and be born at time 0.

    Pacman is born at time 0.
    (Project update: for this question only, [0] and _t are both acceptable.)
    """
    "*** BEGIN YOUR CODE HERE ***"

    A = PropSymbolExpr('PacmanAlive', time=1)
    B = PropSymbolExpr('PacmanAlive', time=0)
    C = PropSymbolExpr('PacmanBorn', time=0)
    D = PropSymbolExpr('PacmanKilled', time=0)

    s1 = A % ((B & ~D) | (~B & C))
    s2 = ~(B & C)
    s3 = C

    return conjoin(s1, s2, s3)

    "*** END YOUR CODE HERE ***"

def findModel(sentence: Expr) -> Dict[Expr, bool]:
    """Given a propositional logic sentence (i.e. a Expr instance), returns a satisfying
    model if one exists. Otherwise, returns False.
    """
    cnf_sentence = to_cnf(sentence)
    return pycoSAT(cnf_sentence)

def findModelCheck() -> Dict[Any, bool]:
    """Returns the result of findModel(Expr('a')) if lower cased expressions were allowed.
    You should not use findModel or Expr in this method.
    This can be solved with a one-line return statement.
    """
    class dummyClass:
        """dummy('A') has representation A, unlike a string 'A' that has repr 'A'.
        Of note: Expr('Name') has representation Name, not 'Name'.
        """
        def __init__(self, variable_name: str = 'A'):
            self.variable_name = variable_name

        def __repr__(self):
            return self.variable_name

    return {dummyClass('a'): True}


def entails(premise: Expr, conclusion: Expr) -> bool:
    """Returns True if the premise entails the conclusion and False otherwise.
    """
    "*** BEGIN YOUR CODE HERE ***"
    return findModel(conjoin(premise, ~conclusion)) is False
    "*** END YOUR CODE HERE ***"

def plTrueInverse(assignments: Dict[Expr, bool], inverse_statement: Expr) -> bool:
    """Returns True if the (not inverse_statement) is True given assignments and False otherwise.
    pl_true may be useful here; see logic.py for its description.
    """
    "*** BEGIN YOUR CODE HERE ***"
    return not pl_true(inverse_statement, assignments)
    "*** END YOUR CODE HERE ***"

#______________________________________________________________________________
# QUESTION 2

def atLeastOne(literals: List[Expr]) -> Expr:
    """
    Given a list of Expr literals (i.e. in the form A or ~A), return a single 
    Expr instance in CNF (conjunctive normal form) that represents the logic 
    that at least one of the literals ist is true.
    >>> A = PropSymbolExpr('A');
    >>> B = PropSymbolExpr('B');
    >>> symbols = [A, B]
    >>> atleast1 = atLeastOne(symbols)
    >>> model1 = {A:False, B:False}
    >>> print(pl_true(atleast1,model1))
    False
    >>> model2 = {A:False, B:True}
    >>> print(pl_true(atleast1,model2))
    True
    >>> model3 = {A:True, B:True}
    >>> print(pl_true(atleast1,model2))
    True
    """
    "*** BEGIN YOUR CODE HERE ***"
    """
    "AtLeastOne" is true if any of the literals is true. Basically,
    it is the disjunction of all the literals, an "OR" operation.
    """
    return disjoin(literals)
    "*** END YOUR CODE HERE ***"


def atMostOne(literals: List[Expr]) -> Expr:
    """
    Given a list of Expr literals, return a single Expr instance in 
    CNF (conjunctive normal form) that represents the logic that at most one of 
    the expressions in the list is true.
    itertools.combinations may be useful here.
    """
    "*** BEGIN YOUR CODE HERE ***"
    """
    "AtMostOne" is true if at most one of the literals is true. Generating
    mutually exclusive pairs of literals and then conjoining them will
    achieve this.
    """

    combinations = itertools.combinations(literals, 2)
    expr_combinations = []

    for A, B in combinations:
        expr_combinations.append(disjoin([~A, ~B]))

    return conjoin(expr_combinations)
    "*** END YOUR CODE HERE ***"


def exactlyOne(literals: List[Expr]) -> Expr:
    """
    Given a list of Expr literals, return a single Expr instance in 
    CNF (conjunctive normal form)that represents the logic that exactly one of 
    the expressions in the list is true.
    """
    "*** BEGIN YOUR CODE HERE ***"
    """
    "ExactlyOne" is true if exactly one of the literals is true. This is just
    the conjunction of "AtLeastOne" and "AtMostOne".
    """
    return conjoin([atLeastOne(literals), atMostOne(literals)])
    "*** END YOUR CODE HERE ***"

#______________________________________________________________________________
# QUESTION 3

def pacmanSuccessorAxiomSingle(x: int, y: int, time: int, walls_grid: List[List[bool]]=None) -> Expr:
    """
    Successor state axiom for state (x,y,t) (from t-1), given the board (as a 
    grid representing the wall locations).
    Current <==> (previous position at time t-1) & (took action to move to x, y)
    Available actions are ['North', 'East', 'South', 'West']
    Note that STOP is not an available action.
    """
    now, last = time, time - 1
    possible_causes: List[Expr] = [] # enumerate all possible causes for P[x,y]_t
    # the if statements give a small performance boost and are required for q4 and q5 correctness
    if walls_grid[x][y+1] != 1:
        possible_causes.append( PropSymbolExpr(pacman_str, x, y+1, time=last)
                            & PropSymbolExpr('South', time=last))
    if walls_grid[x][y-1] != 1:
        possible_causes.append( PropSymbolExpr(pacman_str, x, y-1, time=last) 
                            & PropSymbolExpr('North', time=last))
    if walls_grid[x+1][y] != 1:
        possible_causes.append( PropSymbolExpr(pacman_str, x+1, y, time=last) 
                            & PropSymbolExpr('West', time=last))
    if walls_grid[x-1][y] != 1:
        possible_causes.append( PropSymbolExpr(pacman_str, x-1, y, time=last) 
                            & PropSymbolExpr('East', time=last))
    if not possible_causes:
        return None
    
    "*** BEGIN YOUR CODE HERE ***"
    """
    The successor state axiom is a disjunction of all possible causes for
    the current state. This is due to the current state being true could be
    achieved by any of the possible causes.
    """
    return PropSymbolExpr(pacman_str, x, y, time=now) % disjoin(possible_causes)
    "*** END YOUR CODE HERE ***"


def SLAMSuccessorAxiomSingle(x: int, y: int, time: int, walls_grid: List[List[bool]]) -> Expr:
    """
    Similar to `pacmanSuccessorStateAxioms` but accounts for illegal actions
    where the pacman might not move timestep to timestep.
    Available actions are ['North', 'East', 'South', 'West']
    """
    now, last = time, time - 1
    moved_causes: List[Expr] = [] # enumerate all possible causes for P[x,y]_t, assuming moved to having moved
    if walls_grid[x][y+1] != 1:
        moved_causes.append( PropSymbolExpr(pacman_str, x, y+1, time=last)
                            & PropSymbolExpr('South', time=last))
    if walls_grid[x][y-1] != 1:
        moved_causes.append( PropSymbolExpr(pacman_str, x, y-1, time=last) 
                            & PropSymbolExpr('North', time=last))
    if walls_grid[x+1][y] != 1:
        moved_causes.append( PropSymbolExpr(pacman_str, x+1, y, time=last) 
                            & PropSymbolExpr('West', time=last))
    if walls_grid[x-1][y] != 1:
        moved_causes.append( PropSymbolExpr(pacman_str, x-1, y, time=last) 
                            & PropSymbolExpr('East', time=last))
    if not moved_causes:
        return None

    moved_causes_sent: Expr = conjoin([~PropSymbolExpr(pacman_str, x, y, time=last) , ~PropSymbolExpr(wall_str, x, y), disjoin(moved_causes)])

    failed_move_causes: List[Expr] = [] # using merged variables, improves speed significantly
    auxilary_expression_definitions: List[Expr] = []
    for direction in DIRECTIONS:
        dx, dy = DIR_TO_DXDY_MAP[direction]
        wall_dir_clause = PropSymbolExpr(wall_str, x + dx, y + dy) & PropSymbolExpr(direction, time=last)
        wall_dir_combined_literal = PropSymbolExpr(wall_str + direction, x + dx, y + dy, time=last)
        failed_move_causes.append(wall_dir_combined_literal)
        auxilary_expression_definitions.append(wall_dir_combined_literal % wall_dir_clause)

    failed_move_causes_sent: Expr = conjoin([
        PropSymbolExpr(pacman_str, x, y, time=last),
        disjoin(failed_move_causes)])

    return conjoin([PropSymbolExpr(pacman_str, x, y, time=now) % disjoin([moved_causes_sent, failed_move_causes_sent])] + auxilary_expression_definitions)


def pacphysicsAxioms(t: int, all_coords: List[Tuple], non_outer_wall_coords: List[Tuple], walls_grid: List[List] = None, sensorModel: Callable = None, successorAxioms: Callable = None) -> Expr:
    """
    Given:
        t: timestep
        all_coords: list of (x, y) coordinates of the entire problem
        non_outer_wall_coords: list of (x, y) coordinates of the entire problem,
            excluding the outer border (these are the actual squares pacman can
            possibly be in)
        walls_grid: 2D array of either -1/0/1 or T/F. Used only for successorAxioms.
            Do NOT use this when making possible locations for pacman to be in.
        sensorModel(t, non_outer_wall_coords) -> Expr: function that generates
            the sensor model axioms. If None, it's not provided, so shouldn't be run.
        successorAxioms(t, walls_grid, non_outer_wall_coords) -> Expr: function that generates
            the sensor model axioms. If None, it's not provided, so shouldn't be run.
    Return a logic sentence containing all of the following:
        - for all (x, y) in all_coords:
            If a wall is at (x, y) --> Pacman is not at (x, y)
        - Pacman is at exactly one of the squares at timestep t.
        - Pacman takes exactly one action at timestep t.
        - Results of calling sensorModel(...), unless None.
        - Results of calling successorAxioms(...), describing how Pacman can end in various
            locations on this time step. Consider edge cases. Don't call if None.
    """
    pacphysics_sentences = []

    "*** BEGIN YOUR CODE HERE ***"
    """
    The first sentence is a conjunction of all the locations in the map. If
    there is a wall at a location, then Pacman cannot be at that location.
    """
    pacphysics_sentences.extend(
        PropSymbolExpr(wall_str, x, y) >> ~PropSymbolExpr(pacman_str, x, y, time=t) for x, y in all_coords)

    """
    Pacman could be at exactly one of the squares at timestep t.
    """
    pacphysics_sentences.append(
        exactlyOne([PropSymbolExpr(pacman_str, x, y, time=t) for x, y in non_outer_wall_coords]))

    """
    As well as Pacman taking exactly one action at timestep t.
    """
    pacphysics_sentences.append(
        exactlyOne([PropSymbolExpr(direction, time=t) for direction in DIRECTIONS]))

    """
    The sensorModel is the mechanism by which Pacman can sense the world. It could be
    None, in which case we don't need to add any sentences. Otherwise, we add the
    sentences generated by the sensorModel.
    """
    if sensorModel is not None:
        pacphysics_sentences.append(sensorModel(t, non_outer_wall_coords))

    """
    The successorAxioms are the mechanism by which Pacman can move around the world. It
    could be None, in which case we don't need to add any sentences. Otherwise, we add
    the sentences generated by the successorAxioms.

    Note that successorAxioms may only be called if t > 0. This is because the successor
    axioms are only used to describe how Pacman can move from one location to another
    location. If t = 0, then Pacman is already at the location, so there is no need to
    describe how Pacman can move there.
    """
    if successorAxioms is not None and t > 0:
        pacphysics_sentences.append(successorAxioms(t, walls_grid, non_outer_wall_coords))

    """
    The conjunction of all the sentences is the final result, fully defining the
    Pacman physics for timestep t.
    """
    "*** END YOUR CODE HERE ***"

    return conjoin(pacphysics_sentences)


def checkLocationSatisfiability(x1_y1: Tuple[int, int], x0_y0: Tuple[int, int], action0, action1, problem):
    """
    Given:
        - x1_y1 = (x1, y1), a potential location at time t = 1
        - x0_y0 = (x0, y0), Pacman's location at time t = 0
        - action0 = one of the four items in DIRECTIONS, Pacman's action at time t = 0
        - action1 = to ensure match with autograder solution
        - problem = an instance of logicAgents.LocMapProblem
    Note:
        - there's no sensorModel because we know everything about the world
        - the successorAxioms should be allLegalSuccessorAxioms where needed
    Return:
        - a model where Pacman is at (x1, y1) at time t = 1
        - a model where Pacman is not at (x1, y1) at time t = 1
    """
    walls_grid = problem.walls
    walls_list = walls_grid.asList()
    all_coords = list(itertools.product(range(problem.getWidth()+2), range(problem.getHeight()+2)))
    non_outer_wall_coords = list(itertools.product(range(1, problem.getWidth()+1), range(1, problem.getHeight()+1)))
    KB = []
    x0, y0 = x0_y0
    x1, y1 = x1_y1

    # We know which coords are walls:
    map_sent = [PropSymbolExpr(wall_str, x, y) for x, y in walls_list]
    KB.append(conjoin(map_sent))

    "*** BEGIN YOUR CODE HERE ***"
    """
    This is just a test function which checks whether Pacman can be at a
    particular location at a particular time, given the action taken at the
    previous time. It is used to check whether the successorAxioms are
    correct.
    """
    KB.append(pacphysicsAxioms(0, all_coords, non_outer_wall_coords, walls_grid, None, allLegalSuccessorAxioms))
    KB.append(pacphysicsAxioms(1, all_coords, non_outer_wall_coords, walls_grid, None, allLegalSuccessorAxioms))

    KB.append(PropSymbolExpr(pacman_str, x0, y0, time=0))

    KB.append(PropSymbolExpr(action0, time=0))
    KB.append(PropSymbolExpr(action1, time=1))

    P_1_1_1 = PropSymbolExpr(pacman_str, x1, y1, time=1)

    model1 = findModel(conjoin(conjoin(KB), P_1_1_1))
    model2 = findModel(conjoin(conjoin(KB), ~P_1_1_1))

    return (model1, model2)
    "*** END YOUR CODE HERE ***"

#______________________________________________________________________________
# QUESTION 4

def positionLogicPlan(problem) -> List:
    """
    Given an instance of a PositionPlanningProblem, return a list of actions that lead to the goal.
    Available actions are ['North', 'East', 'South', 'West']
    Note that STOP is not an available action.
    Overview: add knowledge incrementally, and query for a model each timestep. Do NOT use pacphysicsAxioms.
    """
    walls_grid = problem.walls
    width, height = problem.getWidth(), problem.getHeight()
    walls_list = walls_grid.asList()
    x0, y0 = problem.startState
    xg, yg = problem.goal
    
    # Get lists of possible locations (i.e. without walls) and possible actions
    all_coords = list(itertools.product(range(width + 2), 
            range(height + 2)))
    non_wall_coords = [loc for loc in all_coords if loc not in walls_list]
    actions = [ 'North', 'South', 'East', 'West' ]
    KB = []

    "*** BEGIN YOUR CODE HERE ***"
    """
    The walls are known, so we can add them to the KB.
    """
    map_sent = [PropSymbolExpr(wall_str, x, y) for x, y in walls_list]
    KB.append(conjoin(map_sent))

    """
    Similarly, we know that Pacman cannot be at a location if there is a wall.
    """
    not_possible_locations = [~PropSymbolExpr(pacman_str, x, y) for x, y in walls_list]
    KB.append(conjoin(not_possible_locations))

    """
    We know that Pacman starts at the start location.
    """
    KB.append(PropSymbolExpr(pacman_str, x0, y0, time=0))

    for t in range(50):
        print(t)

        """
        Pacman is at exactly one of the squares at timestep t.
        """
        KB.append(
            exactlyOne(
                [PropSymbolExpr(pacman_str, x, y, time=t) for x, y in non_wall_coords]))

        """
        Try to find a model where Pacman is at the goal location at timestep t, given
        the KB. If a model is found, then we can extract the action sequence from the
        model and return it.
        """
        model = findModel(conjoin(conjoin(KB), PropSymbolExpr(pacman_str, xg, yg, time=t)))

        if model is not False:
            return extractActionSequence(model, actions)

        """
        Pacman takes exactly one action at timestep t.
        """
        KB.append(
            exactlyOne(
                [PropSymbolExpr(direction, time=t) for direction in DIRECTIONS]))

        """
        Finally, the KB is extended with the successorAxioms, which describe how Pacman
        can move from one location to another location.
        """
        KB.extend(
            [pacmanSuccessorAxiomSingle(x, y, t+1, walls_grid) for x, y in non_wall_coords])

    "*** END YOUR CODE HERE ***"

#______________________________________________________________________________
# QUESTION 5

def foodLogicPlan(problem) -> List:
    """
    Given an instance of a FoodPlanningProblem, return a list of actions that help Pacman
    eat all of the food.
    Available actions are ['North', 'East', 'South', 'West']
    Note that STOP is not an available action.
    Overview: add knowledge incrementally, and query for a model each timestep. Do NOT use pacphysicsAxioms.
    """
    walls = problem.walls
    width, height = problem.getWidth(), problem.getHeight()
    walls_list = walls.asList()
    (x0, y0), food = problem.start
    food = food.asList()

    # Get lists of possible locations (i.e. without walls) and possible actions
    all_coords = list(itertools.product(range(width + 2), range(height + 2)))

    non_wall_coords = [loc for loc in all_coords if loc not in walls_list]
    actions = [ 'North', 'South', 'East', 'West' ]

    KB = []

    "*** BEGIN YOUR CODE HERE ***"
    """
    The approach is similar to the previous question, except that we need to
    add the food locations to the KB. We also need to add the food goal to the
    KB, so that we can find a model where Pacman is at the goal location at
    timestep t.
    """
    map_sent = [PropSymbolExpr(wall_str, x, y) for x, y in walls_list]
    KB.append(conjoin(map_sent))

    not_possible_locations = [~PropSymbolExpr(pacman_str, x, y) for x, y in walls_list]
    KB.append(conjoin(not_possible_locations))

    """
    The food locations are known, so we can add them to the KB. However, their
    location may be temporal, so we need to add them for all timesteps.
    """
    food_sent = [PropSymbolExpr(food_str, x, y, time=0) for x, y in food]
    KB.append(conjoin(food_sent))

    KB.append(PropSymbolExpr(pacman_str, x0, y0, time=0))

    for t in range(50):
        print(t)
        """
        At each timestep, the goal will be to find a model where there is no food
        left in the original food positions.
        """
        food_goal = [~PropSymbolExpr(food_str, x, y, time=t) for x, y in food]

        KB.append(
            exactlyOne(
                [PropSymbolExpr(pacman_str, x, y, time=t) for x, y in non_wall_coords]))

        """
        The model will be found if given the KB, all the original food locations are
        empty.
        """
        model = findModel(conjoin(conjoin(KB), conjoin(food_goal)))

        if model is not False:
            return extractActionSequence(model, actions)

        KB.append(
            exactlyOne(
                [PropSymbolExpr(direction, time=t) for direction in DIRECTIONS]))

        KB.extend(
            [pacmanSuccessorAxiomSingle(x, y, t+1, walls) for x, y in non_wall_coords])

        """
        The food locations are temporal, so we need to add them to the KB for
        the next timestep. They will be only there if they were there in the
        previous timestep, and Pacman did not eat them. (AKA move to the same location)
        """
        KB.extend(
            [PropSymbolExpr(food_str, x, y, time=t+1) % (PropSymbolExpr(food_str, x, y, time=t) & ~PropSymbolExpr(pacman_str, x, y, time=t)) for x, y in food])
    "*** END YOUR CODE HERE ***"

#______________________________________________________________________________
# AUX FUNCTIONS FOR Q6, Q7, Q8

"""
This are auxiliary functions for the next three questions. They are used to update
the KB with new information, and to find possible locations for Pacman to be in.
"""

def updateKB(KB: List[Expr], agent, t: int, all_coords: List[Tuple], non_outer_wall_coords: List[Tuple], walls_grid: List[List] = None, sensorModel: Callable = None, perceptRules: Callable = None, successorAxioms: Callable = None):
    """
    The KB is updated with the new information from the sensorModel and the perceptRules.
    The KB is also updated with the successorAxioms, which describe how Pacman can move
    from one location to another location.

    All of the new information is parsed as arguments to this method because they may
    differ between the three questions.
    """
    KB.append(pacphysicsAxioms(t, all_coords, non_outer_wall_coords, walls_grid, sensorModel, successorAxioms))
    KB.append(PropSymbolExpr(agent.actions[t], time=t))
    KB.append(perceptRules(t, agent.getPercepts()))

def findPossibleLocations(KB: List[Expr], non_outer_wall_coords: List[Tuple], t: int) -> List[Tuple[int, int]]:
    """
    A list of possible locations for Pacman to be in is returned. This is done by
    iterating through all the locations and checking if Pacman is at that location
    at timestep t.
    """
    possible_locations = []
    for x, y in non_outer_wall_coords:
        pacman_at_xyt = PropSymbolExpr(pacman_str, x, y, time=t)
        pacman_at_xyt_is_entailed_by_kb = entails(conjoin(KB), pacman_at_xyt)
        not_pacman_at_xyt_is_entailed_by_kb = entails(conjoin(KB), ~pacman_at_xyt)

        """
        If Pacman position is entailed by the KB, then Pacman is at that location.
        If not Pacman position is entailed by the KB, then Pacman is not at that
        location.

        The combination of these two statements is a contradiction, and should
        fully define the location of Pacman.
        """

        """
        However, both of them cannot be true at the same time. If they are, then
        there is a contradiction in the KB.
        """
        if pacman_at_xyt_is_entailed_by_kb and not_pacman_at_xyt_is_entailed_by_kb:
            print("Warning: Contradiction detected.")
            continue

        """
        This is the case where Pacman is proven to be at a location.
        """
        if pacman_at_xyt_is_entailed_by_kb:
            KB.append(pacman_at_xyt)

        """
        This is the case where Pacman is proven to not be at a location.

        If this is not the case (AKA Pacman is neither proven to be at a location)
        then we add the location to the list of possible locations. Reaching this point
        means that the location of Pacman is ambiguous, thus the location is possible, but
        not proven.
        """
        if not_pacman_at_xyt_is_entailed_by_kb:
            KB.append(~pacman_at_xyt)
        else:
            possible_locations.append((x, y))

    return possible_locations

def findDemonstrableWalls(KB: List[Expr], known_map: List[List[int]], non_outer_wall_coords: List[Tuple]):
    """
    Similar to the previous method, this method finds the locations of the walls.
    """
    for x, y in non_outer_wall_coords:
        wall_at_xy = PropSymbolExpr(wall_str, x, y)
        wall_at_xy_is_entailed_by_kb = entails(conjoin(KB), wall_at_xy)
        not_wall_at_xy_is_entailed_by_kb = entails(conjoin(KB), ~wall_at_xy)

        """
        If wall position is entailed by the KB, then there is a wall at that location.
        If not wall position is entailed by the KB, then there is not a wall at that
        location.

        The combination of these two statements is a contradiction, and should
        fully define the location of the wall.
        """

        """
        However, both of them cannot be true at the same time. If they are, then
        there is a contradiction in the KB. The location is marked as unknown.
        """
        if wall_at_xy_is_entailed_by_kb and not_wall_at_xy_is_entailed_by_kb:
            print("Warning: Contradiction detected.")
            known_map[x][y] = -1
            continue

        """
        This is the case where the presence or absence of a wall is not proven. Thus
        the location is marked as unknown.
        """
        if not wall_at_xy_is_entailed_by_kb and not not_wall_at_xy_is_entailed_by_kb:
            known_map[x][y] = -1
            continue

        """
        If the location was entailed by the KB, then there is a wall at that location.
        """
        if wall_at_xy_is_entailed_by_kb and not not_wall_at_xy_is_entailed_by_kb:
            KB.append(PropSymbolExpr(wall_str, x, y))
            known_map[x][y] = 1
            continue

        """
        If the location was not entailed by the KB, then there is not a wall at that
        location.
        """
        if not_wall_at_xy_is_entailed_by_kb and not wall_at_xy_is_entailed_by_kb:
            KB.append(~PropSymbolExpr(wall_str, x, y))
            known_map[x][y] = 0

#______________________________________________________________________________
# QUESTION 6

def localization(problem, agent) -> Generator:
    '''
    problem: a LocalizationProblem instance
    agent: a LocalizationLogicAgent instance
    '''
    walls_grid = problem.walls
    walls_list = walls_grid.asList()
    all_coords = list(itertools.product(range(problem.getWidth()+2), range(problem.getHeight()+2)))
    non_outer_wall_coords = list(itertools.product(range(1, problem.getWidth()+1), range(1, problem.getHeight()+1)))

    KB = []

    "*** BEGIN YOUR CODE HERE ***"
    """
    The walls are known, so we can add them to the KB, either the presence or
    absence of a wall.
    """
    KB.extend([PropSymbolExpr(wall_str, x, y) if (x, y) in walls_list else ~PropSymbolExpr(wall_str, x, y) for x, y in all_coords])

    for t in range(agent.num_timesteps):
        """
        The KB is updated with the new information from the sensorModel and the perceptRules. Also, the KB is updated with the successorAxioms, which describe how Pacman can move from one location to another location.
        """
        updateKB(KB, agent, t, all_coords, non_outer_wall_coords, walls_grid, sensorAxioms, fourBitPerceptRules, allLegalSuccessorAxioms)

        """
        Once the KB is updated, the possible location of Pacman could be found.
        """
        possible_locations = findPossibleLocations(KB, non_outer_wall_coords, t)

        agent.moveToNextState(agent.actions[t])
        "*** END YOUR CODE HERE ***"
        yield possible_locations

#______________________________________________________________________________
# QUESTION 7

def mapping(problem, agent) -> Generator:
    '''
    problem: a MappingProblem instance
    agent: a MappingLogicAgent instance
    '''
    pac_x_0, pac_y_0 = problem.startState
    KB = []
    all_coords = list(itertools.product(range(problem.getWidth()+2), range(problem.getHeight()+2)))
    non_outer_wall_coords = list(itertools.product(range(1, problem.getWidth()+1), range(1, problem.getHeight()+1)))

    # map describes what we know, for GUI rendering purposes. -1 is unknown, 0 is open, 1 is wall
    known_map = [[-1 for y in range(problem.getHeight()+2)] for x in range(problem.getWidth()+2)]

    # Pacman knows that the outer border of squares are all walls
    outer_wall_sent = []
    for x, y in all_coords:
        if ((x == 0 or x == problem.getWidth() + 1)
                or (y == 0 or y == problem.getHeight() + 1)):
            known_map[x][y] = 1
            outer_wall_sent.append(PropSymbolExpr(wall_str, x, y))
    KB.append(conjoin(outer_wall_sent))

    "*** BEGIN YOUR CODE HERE ***"
    """
    The start location is known, so we can add it to the KB.
    This also implies that there is no wall at the start location.
    """
    KB.append(PropSymbolExpr(pacman_str, pac_x_0, pac_y_0, time=0))
    KB.append(~PropSymbolExpr(wall_str, pac_x_0, pac_y_0))
    known_map[pac_x_0][pac_y_0] = 0

    for t in range(agent.num_timesteps):
        """
        The KB is updated with the new information from the sensorModel and the perceptRules. Also, the KB is updated with the successorAxioms, which describe how Pacman can move from one location to another location.
        """
        updateKB(KB, agent, t, all_coords, non_outer_wall_coords, known_map, sensorAxioms, fourBitPerceptRules, allLegalSuccessorAxioms)

        """
        Once the KB is updated, the demonstrable walls location could be determined.
        """
        findDemonstrableWalls(KB, known_map, non_outer_wall_coords)

        agent.moveToNextState(agent.actions[t])
        "*** END YOUR CODE HERE ***"
        yield known_map

#______________________________________________________________________________
# QUESTION 8

def slam(problem, agent) -> Generator:
    '''
    problem: a SLAMProblem instance
    agent: a SLAMLogicAgent instance
    '''
    pac_x_0, pac_y_0 = problem.startState
    KB = []
    all_coords = list(itertools.product(range(problem.getWidth()+2), range(problem.getHeight()+2)))
    non_outer_wall_coords = list(itertools.product(range(1, problem.getWidth()+1), range(1, problem.getHeight()+1)))

    # map describes what we know, for GUI rendering purposes. -1 is unknown, 0 is open, 1 is wall
    known_map = [[-1 for y in range(problem.getHeight()+2)] for x in range(problem.getWidth()+2)]

    # We know that the outer_coords are all walls.
    outer_wall_sent = []
    for x, y in all_coords:
        if ((x == 0 or x == problem.getWidth() + 1)
                or (y == 0 or y == problem.getHeight() + 1)):
            known_map[x][y] = 1
            outer_wall_sent.append(PropSymbolExpr(wall_str, x, y))
    KB.append(conjoin(outer_wall_sent))

    "*** BEGIN YOUR CODE HERE ***"
    """
    Similar to the previous question, the start location is known, so we can
    add it to the KB. This also implies that there is no wall at the start
    location.

    However, the rest of the walls are unknown.
    """
    KB.append(PropSymbolExpr(pacman_str, pac_x_0, pac_y_0, time=0))
    KB.append(~PropSymbolExpr(wall_str, pac_x_0, pac_y_0))
    known_map[pac_x_0][pac_y_0] = 0

    for t in range(agent.num_timesteps):
        """
        The KB is updated with the new information from the sensorModel and the perceptRules. Also, the KB is updated with the successorAxioms, which describe how Pacman can move from one location to another location.
        """
        updateKB(KB, agent, t, all_coords, non_outer_wall_coords, known_map, SLAMSensorAxioms, numAdjWallsPerceptRules, SLAMSuccessorAxioms)

        """
        Similarly to previous questions, the demonstrable walls location could be
        determined, as well as the possible locations of Pacman.
        """
        findDemonstrableWalls(KB, known_map, non_outer_wall_coords)

        possible_locations = findPossibleLocations(KB, non_outer_wall_coords, t)

        agent.moveToNextState(agent.actions[t])
        "*** END YOUR CODE HERE ***"
        yield (known_map, possible_locations)


# Abbreviations
plp = positionLogicPlan
loc = localization
mp = mapping
flp = foodLogicPlan
# Sometimes the logic module uses pretty deep recursion on long expressions
sys.setrecursionlimit(100000)

#______________________________________________________________________________
# Important expression generating functions, useful to read for understanding of this project.


def sensorAxioms(t: int, non_outer_wall_coords: List[Tuple[int, int]]) -> Expr:
    all_percept_exprs = []
    combo_var_def_exprs = []
    for direction in DIRECTIONS:
        percept_exprs = []
        dx, dy = DIR_TO_DXDY_MAP[direction]
        for x, y in non_outer_wall_coords:
            combo_var = PropSymbolExpr(pacman_wall_str, x, y, x + dx, y + dy, time=t)
            percept_exprs.append(combo_var)
            combo_var_def_exprs.append(combo_var % (
                PropSymbolExpr(pacman_str, x, y, time=t) & PropSymbolExpr(wall_str, x + dx, y + dy)))

        percept_unit_clause = PropSymbolExpr(blocked_str_map[direction], time = t)
        all_percept_exprs.append(percept_unit_clause % disjoin(percept_exprs))

    return conjoin(all_percept_exprs + combo_var_def_exprs)


def fourBitPerceptRules(t: int, percepts: List) -> Expr:
    """
    Localization and Mapping both use the 4 bit sensor, which tells us True/False whether
    a wall is to pacman's north, south, east, and west.
    """
    assert isinstance(percepts, list), "Percepts must be a list."
    assert len(percepts) == 4, "Percepts must be a length 4 list."

    percept_unit_clauses = []
    for wall_present, direction in zip(percepts, DIRECTIONS):
        percept_unit_clause = PropSymbolExpr(blocked_str_map[direction], time=t)
        if not wall_present:
            percept_unit_clause = ~PropSymbolExpr(blocked_str_map[direction], time=t)
        percept_unit_clauses.append(percept_unit_clause) # The actual sensor readings
    return conjoin(percept_unit_clauses)


def numAdjWallsPerceptRules(t: int, percepts: List) -> Expr:
    """
    SLAM uses a weaker numAdjWallsPerceptRules sensor, which tells us how many walls pacman is adjacent to
    in its four directions.
        000 = 0 adj walls.
        100 = 1 adj wall.
        110 = 2 adj walls.
        111 = 3 adj walls.
    """
    assert isinstance(percepts, list), "Percepts must be a list."
    assert len(percepts) == 3, "Percepts must be a length 3 list."

    percept_unit_clauses = []
    for i, percept in enumerate(percepts):
        n = i + 1
        percept_literal_n = PropSymbolExpr(geq_num_adj_wall_str_map[n], time=t)
        if not percept:
            percept_literal_n = ~percept_literal_n
        percept_unit_clauses.append(percept_literal_n)
    return conjoin(percept_unit_clauses)


def SLAMSensorAxioms(t: int, non_outer_wall_coords: List[Tuple[int, int]]) -> Expr:
    all_percept_exprs = []
    combo_var_def_exprs = []
    for direction in DIRECTIONS:
        percept_exprs = []
        dx, dy = DIR_TO_DXDY_MAP[direction]
        for x, y in non_outer_wall_coords:
            combo_var = PropSymbolExpr(pacman_wall_str, x, y, x + dx, y + dy, time=t)
            percept_exprs.append(combo_var)
            combo_var_def_exprs.append(combo_var % (PropSymbolExpr(pacman_str, x, y, time=t) & PropSymbolExpr(wall_str, x + dx, y + dy)))

        blocked_dir_clause = PropSymbolExpr(blocked_str_map[direction], time=t)
        all_percept_exprs.append(blocked_dir_clause % disjoin(percept_exprs))

    percept_to_blocked_sent = []
    for n in range(1, 4):
        wall_combos_size_n = itertools.combinations(blocked_str_map.values(), n)
        n_walls_blocked_sent = disjoin([
            conjoin([PropSymbolExpr(blocked_str, time=t) for blocked_str in wall_combo])
            for wall_combo in wall_combos_size_n])
        # n_walls_blocked_sent is of form: (N & S) | (N & E) | ...
        percept_to_blocked_sent.append(
            PropSymbolExpr(geq_num_adj_wall_str_map[n], time=t) % n_walls_blocked_sent)

    return conjoin(all_percept_exprs + combo_var_def_exprs + percept_to_blocked_sent)


def allLegalSuccessorAxioms(t: int, walls_grid: List[List], non_outer_wall_coords: List[Tuple[int, int]]) -> Expr:
    """walls_grid can be a 2D array of ints or bools."""
    all_xy_succ_axioms = []
    for x, y in non_outer_wall_coords:
        xy_succ_axiom = pacmanSuccessorAxiomSingle(
            x, y, t, walls_grid)
        if xy_succ_axiom:
            all_xy_succ_axioms.append(xy_succ_axiom)
    return conjoin(all_xy_succ_axioms)


def SLAMSuccessorAxioms(t: int, walls_grid: List[List], non_outer_wall_coords: List[Tuple[int, int]]) -> Expr:
    """walls_grid can be a 2D array of ints or bools."""
    all_xy_succ_axioms = []
    for x, y in non_outer_wall_coords:
        xy_succ_axiom = SLAMSuccessorAxiomSingle(
            x, y, t, walls_grid)
        if xy_succ_axiom:
            all_xy_succ_axioms.append(xy_succ_axiom)
    return conjoin(all_xy_succ_axioms)

#______________________________________________________________________________
# Various useful functions, are not needed for completing the project but may be useful for debugging


def modelToString(model: Dict[Expr, bool]) -> str:
    """Converts the model to a string for printing purposes. The keys of a model are 
    sorted before converting the model to a string.
    
    model: Either a boolean False or a dictionary of Expr symbols (keys) 
    and a corresponding assignment of True or False (values). This model is the output of 
    a call to pycoSAT.
    """
    if model == False:
        return "False" 
    else:
        # Dictionary
        modelList = sorted(model.items(), key=lambda item: str(item[0]))
        return str(modelList)


def extractActionSequence(model: Dict[Expr, bool], actions: List) -> List:
    """
    Convert a model in to an ordered list of actions.
    model: Propositional logic model stored as a dictionary with keys being
    the symbol strings and values being Boolean: True or False
    Example:
    >>> model = {"North[2]":True, "P[3,4,0]":True, "P[3,3,0]":False, "West[0]":True, "GhostScary":True, "West[2]":False, "South[1]":True, "East[0]":False}
    >>> actions = ['North', 'South', 'East', 'West']
    >>> plan = extractActionSequence(model, actions)
    >>> print(plan)
    ['West', 'South', 'North']
    """
    plan = [None for _ in range(len(model))]
    for sym, val in model.items():
        parsed = parseExpr(sym)
        if type(parsed) == tuple and parsed[0] in actions and val:
            action, _, time = parsed
            plan[time] = action
    #return list(filter(lambda x: x is not None, plan))
    return [x for x in plan if x is not None]


# Helpful Debug Method
def visualizeCoords(coords_list, problem) -> None:
    wallGrid = game.Grid(problem.walls.width, problem.walls.height, initialValue=False)
    for (x, y) in itertools.product(range(problem.getWidth()+2), range(problem.getHeight()+2)):
        if (x, y) in coords_list:
            wallGrid.data[x][y] = True
    print(wallGrid)


# Helpful Debug Method
def visualizeBoolArray(bool_arr, problem) -> None:
    wallGrid = game.Grid(problem.walls.width, problem.walls.height, initialValue=False)
    wallGrid.data = copy.deepcopy(bool_arr)
    print(wallGrid)

class PlanningProblem:
    """
    This class outlines the structure of a planning problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the planning problem.
        """
        util.raiseNotDefined()

    def getGhostStartStates(self):
        """
        Returns a list containing the start state for each ghost.
        Only used in problems that use ghosts (FoodGhostPlanningProblem)
        """
        util.raiseNotDefined()
        
    def getGoalState(self):
        """
        Returns goal state for problem. Note only defined for problems that have
        a unique goal state such as PositionPlanningProblem
        """
        util.raiseNotDefined()

#______________________________________________________________________________
# MODIFICACIÓN 1

def noneIsTrue(literals: List[Expr]) -> Expr:
    """
    Given a list of Expr literals, return a single Expr instance in 
    CNF (conjunctive normal form) that represents the logic that none of 
    the expressions in the list is true.
    """
    "*** BEGIN YOUR CODE HERE ***"
    """
    "noneIsTrue" is true if none of the literals is true. This is just
    the negation of "atLeastOne". If at least one is true, then none all
    of them are true. If at lest one is not true, then all of them are false.
    """
    return ~atLeastOne(literals)
    "*** END YOUR CODE HERE ***"

def logicWumpus():
    """
    The str representation for readability.
    """
    pit_str = "Pit"
    breeze_str = "Breeze"

    """
    Each symbol is constructed as requested.
    """
    Pit_1_1 = PropSymbolExpr(pit_str, 1, 1)
    Pit_1_2 = PropSymbolExpr(pit_str, 1, 2)
    Pit_2_1 = PropSymbolExpr(pit_str, 2, 1)
    Pit_2_2 = PropSymbolExpr(pit_str, 2, 2)
    Pit_3_1 = PropSymbolExpr(pit_str, 3, 1)
    Breeze_1_1 = PropSymbolExpr(breeze_str, 1, 1)
    Breeze_2_1 = PropSymbolExpr(breeze_str, 2, 1)

    """
    The KB is initialized empty
    """
    KB = []

    """
    Each statement is constructed as requested.
    """
    s1 = to_cnf(Breeze_1_1 % (disjoin(Pit_1_2, Pit_2_1)))
    s2 = to_cnf(Breeze_2_1 % (disjoin(Pit_1_1, Pit_2_2, Pit_3_1)))
    s3 = ~Pit_1_1
    s4 = ~Pit_2_1
    s5 = Breeze_1_1
    s6 = ~Breeze_2_1

    """
    And they are added to the KB.
    """
    KB.append(conjoin(s1, s2, s3, s4, s5, s6))

    """
    The presence of a Pit at (1, 2) is granted in the KB
    entails so, at the same time that entails the absence of the
    same pit at the same location.
    """
    pit_at_1_2 = entails(conjoin(KB), Pit_1_2)
    not_pit_at_1_2 = entails(conjoin(KB), ~Pit_1_2)

    if pit_at_1_2 and not_pit_at_1_2:
        print("There is contradiction in the KB!")
    elif pit_at_1_2 and not not_pit_at_1_2:
        print("There is a pit at (1, 2)")
    elif not pit_at_1_2 and not_pit_at_1_2:
        print("There is not at pit at (1, 2)")
    else:
        print("I do not know if there is a pit at (1, 2)")

    """
    Try to find a model where none of the pits are true.

    In theory, since the KB implies that there is a pit at
    (1, 2), no model should be found.
    """
    model = findModel(conjoin(conjoin(KB), noneIsTrue(disjoin(Pit_1_2, Pit_2_2))))

    if model:
        print("There is a model where there is not a pit neither at (1, 2) and (2, 2)")
    else:
        print("There is not a model where there is not a pit neither at (1, 2) and (2, 2)")

#______________________________________________________________________________
# MODIFICACIÓN 2

def updateCornersGoal(goal: List[List], corners: List[Tuple], t: int):
    """
    For every corner, a new symbol is constructed, representing the presence of Pacman
    on each of them at the timestep t.

    They are appended to its corresponding list of corners.
    """
    for i, corner in enumerate(corners):
        x, y = corner[0], corner[1]
        goal[i].append(PropSymbolExpr(pacman_str, x, y, time=t))

def goalToExpr(goal: List[List]) -> Expr:
    """
    The goal is represented as a nested list:

        - One element per corner.
        - Each one of said elements contains another list with each corner in each T.
    
    The final Expr is calculated with the conjoin of all the four elements. As well as
    each element is calculated with the disjoin of themselves.
    """
    return conjoin([disjoin(cornerList) for cornerList in goal])

def cornerVisitingPlan(problem) -> List:
    """
    Given an instance of a PositionPlanningProblem, return a list of actions that lead to the goal.
    Available actions are ['North', 'East', 'South', 'West']
    Note that STOP is not an available action.
    Overview: add knowledge incrementally, and query for a model each timestep. Do NOT use pacphysicsAxioms.
    """
    walls_grid = problem.walls
    width, height = problem.getWidth(), problem.getHeight()
    walls_list = walls_grid.asList()
    x0, y0 = problem.startState
    xg, yg = problem.goal
    
    # Get lists of possible locations (i.e. without walls) and possible actions
    all_coords = list(itertools.product(range(width + 2), 
            range(height + 2)))
    non_wall_coords = [loc for loc in all_coords if loc not in walls_list]
    actions = [ 'North', 'South', 'East', 'West' ]
    KB = []

    "*** BEGIN YOUR CODE HERE ***"
    """
    The reason why this plan was modified from the original positionLogicPlan is because
    the goal of the exercise is basically identical to the original plan. The key difference
    is that now the goal is not only an specific location, but the evidence of Pacman has
    visited some of them.

    In consequence, the goal should be reformulated in every iteration, similar to the foodLogicPlan,
    since there is a viable chance of Pacman visiting a corner in each one of them, thus the need
    of keeping track of the previous iterations.

    This means that the only piece of code that needs to be changed is the goal (Consequently, new
    code is added in order to determine the new goal), however, the rest of the algorithm remains
    untouched.
    """

    """
    Both width and height are the inner part of the maze, those represent the topmost reachable
    square. Similarly, (1, 1) represents the bottommost reachable, since the maze is constructed with an outer
    wall. i.e, below is a maze with width = 5, height = 5, whereas the reachable corners are represented with
    a, b, c and d.

    a = 1, height(5)
    b = width(5), height(5)
    c = 1, 1
    d = width(5), 1

    XXXXXXX
    Xa---bX
    X-----X
    X-----X
    X-----X
    Xc---dX
    XXXXXXX
    """
    corners = [(1,1), (1, height), (width, 1), (width, height)]
    goal = [[], [], [], []]

    """
    The walls are known, so we can add them to the KB.
    """
    map_sent = [PropSymbolExpr(wall_str, x, y) for x, y in walls_list]
    KB.append(conjoin(map_sent))

    """
    Similarly, we know that Pacman cannot be at a location if there is a wall.
    """
    not_possible_locations = [~PropSymbolExpr(pacman_str, x, y) for x, y in walls_list]
    KB.append(conjoin(not_possible_locations))

    """
    We know that Pacman starts at the start location.
    """
    KB.append(PropSymbolExpr(pacman_str, x0, y0, time=0))

    for t in range(50):
        print(t)

        """
        The list of goals is updated with the current timestep.
        """
        updateCornersGoal(goal, corners, t)

        """
        Pacman is at exactly one of the squares at timestep t.
        """
        KB.append(
            exactlyOne(
                [PropSymbolExpr(pacman_str, x, y, time=t) for x, y in non_wall_coords]))

        """
        Try to find a model where Pacman is at the goal location at timestep t, given
        the KB. If a model is found, then we can extract the action sequence from the
        model and return it.
        """
        model = findModel(conjoin(conjoin(KB), goalToExpr(goal)))

        if model is not False:
            return extractActionSequence(model, actions)

        """
        Pacman takes exactly one action at timestep t.
        """
        KB.append(
            exactlyOne(
                [PropSymbolExpr(direction, time=t) for direction in DIRECTIONS]))

        """
        Finally, the KB is extended with the successorAxioms, which describe how Pacman
        can move from one location to another location.
        """
        KB.extend(
            [pacmanSuccessorAxiomSingle(x, y, t+1, walls_grid) for x, y in non_wall_coords])

    "*** END YOUR CODE HERE ***"